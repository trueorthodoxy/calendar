---
title: "Июнь 23"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, Собор Еяже иконы Владимирския празднуем (1480)",
"Святыя мученицы Агриппины Римляныни (253-260)",
"Святых священномучеников Аристоклия пресвитера, Димитриана диакона и Афанасия чтеца, Кипрских (ок. 306)",
"Святаго мученика Евстохия, пресвитера Писидийскаго, Гаия, сродника его и чад его: Лоллия, Провия и Урвана, иже во Анкире во главу усеченных (IV)",
"Преподобныя матере нашея Этельдреды девы, игуменьи Элийския (679)",
"Праведнаго отрока Артемия Веркольскаго (1545)",
"Иже во святых отца нашего Германа, архиепископа Казанскаго, егоже святых мощей память пренесения совершаем (1714)",
"Преподобных отцев наших Иосифа (1612), Антония и Иоанникия (XVII), Вологодских",
"Святаго Михаила Клопскаго, Христа ради юродиваго, егоже святых мощей память пренесения совершаем (ок. 1453-1456)",
"Святых новосвященномучеников Митрофана (Краснопольскаго), архиепископа Астраханскаго и Леонтия (Вимпфена), епископа Енотаевскаго (1919)",
"Святаго новосвященномученика Максима (Жижиленко), епископа Серпуховскаго, перваго епископа катакомбныя церкве в России (1931)"]
---
