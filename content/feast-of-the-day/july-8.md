---
title: "Июль 8"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, Собор Еяже иконы Казанския празднуем (1579)",
"Святаго великомученика Прокопия Кесарийскаго и иже с ним во главу усеченных: матере его святыя Феодоры, 12 жен синклитик, Антиоха и Никострата трибунов, Авды и Саввы (303)",
"Преподобнаго отца нашего Феофила Мироточиваго, Афонскаго (1548)",
"Святаго новосвященномученика Анастасия Янинскаго (из села Святаго Власия близ Игуменицы), Константинопольскаго (1743)",
"Преподобныя матере нашея Витбурги Норфолкския (ок. 743)",
"Святыя мученицы девы Суннивы Норвежския (Х)",
"Блаженнаго Прокопия Устюжскаго Христа ради юродиваго, чудотворца (1303)",
"Святаго праведнаго Прокопия Устьянскаго (XVII)",
"Святаго новосвященномученика Прокопия (Титова), архиепископа Херсонскаго (1937)"]
---
