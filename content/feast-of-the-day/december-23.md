---
title: "Декабрь 23"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых мученик Критских: Феодула, Саторнина, Евпора, Геласия, Евникиана, Зотика, Помпия, Агафопуса, Василида и Евареста (ок. 250)",
"Преподобнаго отца нашего Павла, архиепископа Неокесарийскаго, исповедника (IV)",
"Святаго равноапостольнаго Наума Охридскаго, чудотворца, просветителя Болгарскаго (910)",
"Святаго мученика Схинона",
"Иже во святых отца нашего Нифонта, епископа Кипрскаго (IV)",
"Святаго преподобномученика Давида Двинскаго, Армянскаго (ок. 703)",
"Иже во святых отца нашего Феоктиста, архиепископа Новгородскаго (1310)",
"Святых священномучеников Иоанна и Николая, пресвитеров Пермских"]
---
