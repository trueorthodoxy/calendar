---
title: "Май 10"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго Апостола Симона Зилота (I)",
"Святых мученик Сицилийских: Алфия, Филадельфа, Киприана (251)",
"Преподобнаго отца нашего Исихия, исповедника Галатийскаго (790)",
"Преподобнаго отца нашего Лаврентия",
"Преподобнаго отца нашего Комгалла, игумена Бангорскаго, Ирландскаго (ок. 603)",
"Святителя Симона, чудотворца Киево-Печерскаго, епископа Владимирскаго и Суздальскаго (1226)",
"Блаженнаго Симона Юрьевецкаго, Христа ради юродиваго (1584)",
"Святаго мученика Василия девственника, Мангазейскаго чудотворца (1602)",
"Множество святых новопреподобномучеников святыя Симоно-Кананитския обители (Новый Афон), яже в горах Кавказских (1921-1930)"]
---
