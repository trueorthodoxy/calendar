---
title: "Июль 19"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобныя матере нашея Макрины (380), сестры Василия Великаго, и матере их Емилии (IV)",
"Преподобнаго отца нашего Дия Чудотворца, игумена Константинопольскаго (ок. 430)",
"Преподобнаго отца нашего Серафима, Саровскаго чудотворца, егоже святых мощей память обретения совершаем (1903)",
"Святыя четверицы подвижников",
"Иже во святых отца нашего Феодора Савваита, архиепископа Едесскаго (848)",
"Преподобнаго отца нашего Диоклея Фиваидскаго (IV)",
"Святаго Григория Нового исповедника, епископа Панедскаго (VIII-IX)",
"Святых дев мучениц Иусты и Руфины Севильских (287)",
"Святыя девы мученицы Ауреи Кордовския (856)",
"Благовернаго князя Романа Олеговича, Рязанскаго (1270)",
"Преподобнаго отца нашего Паисия Киево-Печерскаго (XIV)",
"Святаго Стефана, деспота Сербскаго (1427) и матере его, святыя Милицы (1405)"]
---
