---
title: "Март 21"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобнаго отца нашего Иакова, епископа и исповедника",
"Иже во святых отца нашего Фомы, патриарха Константинопольскаго",
"Святых мученик Филимона и Домнина Римских",
"Святаго Кирилла, епископа Катанскаго, ученика Святаго Апостола Петра",
"Преподобнаго отца нашего Серапиона Синдонита Египетскаго, в Риме почившаго",
"Святаго Серапиона, епископа Тмуитскаго, в Нижнем Египте",
"Преподобнаго отца нашего Люпикина Кондатскаго, игумена в горах Юры, яже в Галлии",
"Преподобнаго отца нашего Энды, игумена Инишморскаго, Киллеани в Голуэй во Ирландии"]
---
