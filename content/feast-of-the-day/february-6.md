---
title: "Февраль 6"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобнаго отца нашего Вукола, епископа Смирнскаго (ок. 100-105)",
"Святаго мученика Иулиана Емесскаго (312)",
"Святых мученик Фавсты, Евиласия и Максима Кизических (ок. 305-311)",
"Иже во святых отца нашего Фотия исповедника, патриарха Константинопольскаго, равноапостольнаго (891)",
"Святых мученик Фавста, Василия, Силуана (Лукиана) и мученик Дарионских в Константинополе",
"Преподобных отцев наших Варсонофия Великаго и Иоанна Пророка, ученика его (VI)",
"Преподобнаго отца нашего Иоанна Ликонскаго (IV)",
"Преподобнаго отца нашего Иакова Сирийскаго (ок. 460)",
"Святых мученик Дорофеи, Христины, Каллисты и Феофила из Кесарии Каппадокийския (ок. 288-300)",
"Преподобнаго отца нашего Маеля, епископа Ардахскаго во Ирландии (488)",
"Святителя Ведаста, епископа Аррасскаго в Галлии (540)",
"Иже во святых отца нашего Аманда, епископа Маастрихтскаго, просветителя Фландрии (ок. 676-684)",
"Преподобнаго отца нашего Арсения Икалтойскаго во Иверии (1127)",
"Преподобныя матере нашея Дорофеи Кашинския (1629)"]
---
