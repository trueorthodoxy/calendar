---
title: "Июнь 3"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, Собор Еяже иконы Югския празднуем (1615)",
"Святаго мученика Лукиллиана Никомидийскаго, и чад его: Клавдия, Ипатия, Павла, Дионисия и Павлы девы (ок. 270-275)",
"Преподобнаго отца нашего Афанасия Чудотворца, Вифинскаго (925)",
"Преподобныя матере нашея Иерии Нисибинския, Месопотамския (ок. 320)",
"Преподобнаго отца нашего Паппия (Паппа), епископа Хитрскаго (368)",
"Святаго священномученика Лукиана епископа и иже с ним Максима пресвитера и Иулиана диакона, Бовейских (Бельгийских) в Галлии (81-96)",
"Праведныя Клотильды, Царицы Галльския (545)",
"Преподобнаго отца нашего Кевина, игумена Глендалохскаго, чудотворца Ирландскаго (ок. 618)",
"Святаго преподобномученика Исаака Кордовскаго (851-852)",
"Благовернаго царевича Димитрия Угличскаго, Московскаго, егоже святых мощей память пренесения совершаем (1606)"]
---
