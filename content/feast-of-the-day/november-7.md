---
title: "Ноябрь 7"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых мученик Мелитинских: Иерона, Аникиты, Афанасия, Варахия, Каллимаха, Каллиника, Кастрикия, Клавдиана, Дорофея, Дукития, Епифания, Евгения, Евтихия, Гигантия, Исихия, Лонгина, Маманта, Максимиана, Никандра, Никона, Острихия, Феогена, Фемелия, Феодоха, Феодора, Феодота, Феодула, Феофила, Валерия, Ксанфа, Илариона, Диодота и Амонита (290)",
"Преподобнаго отца нашего Лазаря, чудотворца Галисийскаго (1053-1054)",
"Святых мученик Анкирских: Меласиппа, Касинии, сына их Антонина и сорока отроков (363)",
"Святых мученик Авкта, Тавриона и Фессалоникии Амфипольских в Македонии",
"Святителя Афинодора, епископа Амасийскаго, брата святителя Григория Чудотворца (ок. 270)",
"Святаго мученика Александра Солунскаго (305)",
"Иже во святых отца нашего Виллиброрда Нортумбрийскаго, епископа Утрехтскаго, просветителя фризов (738-739)",
"Преподобнаго отца нашего Зосимы Ворбозомскаго (ок. 1550)",
"Преподобнаго отца нашего Кирилла Новоезерскаго, егоже святых мощей память обретения совершаем (1649)",
"Святых новосвященномучеников Кирилла (Смирнова), митрополита Казанскаго (1937) и Иосифа (Петровых), митрополита Петроградскаго (1937)"]
---
