---
title: "Май 26"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго и славнаго Апостола от 70 Карпа (I)",
"Святаго Апостола от 70 Алфея и мученик Аверкия и Елены, чад его (I)",
"Святаго новомученика Александра Дервиша, Солунскаго, иже в Смирне во главу усеченнаго (1794)",
"Святителя Августина, перваго архиепископа Кентерберийскаго (605)",
"Преподобнаго и богоноснаго отца нашего Синесия, епископа Карпасийскаго, Кипрскаго (V)",
"Преподобнаго отца нашего Макария Калязинскаго, егоже святых мощей память обретения совершаем (1521)",
"Святаго новомученика Георгия Болгарскаго"]
---
