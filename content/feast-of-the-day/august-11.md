---
title: "Август 11"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго великомученика Евпла, диакона Катанскаго, в Сицилии (304)",
"Святых мученик Неофита, Гаия, Гаиана, Зинона, Марка и Макария",
"Преподобнаго отца нашего Пассариона Палестинскаго (428)",
"Преподобнаго отца нашего Нифонта, патриарха Константинопольскаго, иже во обители Дионисиат (1508)",
"Святителя Спиридона чудотворца, егоже чуда со агарянами на Корфу память совершаем (1716)",
"Святыя мученицы Сосанны девы и иже с нею мученик Гаия папы Римскаго, Гавиния пресвитера, Максима, Клавдия, Препедигны, Александра и Куфия (295-296)",
"Святаго мученика Елиана Декаполита",
"Святаго Блейна, епископа Бьютскаго в Шотландии (ок. 590)",
"Святых преподобномучеников Василия и Феодора Киево-Печерских (1098)",
"Святаго Феодора, князя Острожскаго, во иночестве Феодосия, Киево-Печерскаго (XV)",
"Святых новомучеников Анастасия и Димитрия Лесбосских (1816 или 1819)"]
---
