---
title: "Февраль 18"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Иже во святых отца нашего Льва Великаго, папы Римскаго (461)",
"Святых мученик Льва и Паригория Патарских (ок. 258-260)",
"Преподобнаго отца нашего Агапита, исповедника и чудотворца, епископа Синадскаго, и святых мученик Виктора, Дорофея, Феодула и Агриппы (IV)",
"Святаго мученика Пиулия",
"Преподобнаго отца нашего Космы Яхромскаго (1492)",
"Святых новосвященномучеников Льва (Кунцевича), миссионера Воронежскаго (1918) и Агапита (Вишневскаго), архиепископа Екатеринославскаго (1925-1926)"]
---
