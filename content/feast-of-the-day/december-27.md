---
title: "Декабрь 27"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго апостола и первомученика архидиакона Стефана (ок. 34)",
"Преподобнаго отца нашего Феодора Начертаннаго (ок. 840), брата святаго Феофана Начертаннаго",
"Преподобнаго отца нашего Феодора, архиепископа Константинопольскаго (ок. 686)",
"Святаго мученика Маврикия Апамейскаго и с ним 70 мученик (305)",
"Преподобнаго отца нашего Луки Триглийскаго (Глубокореченскаго) (X)",
"Святаго новосвященномученика Тихона (Никанорова), архиепископа Воронежскаго и с ним 160 пресвитеров (1919)",
"Святаго новосвященномученика Леонида (Серебреникова), пресвитера Лермонтовскаго, Хабаровскаго (1919)"]
---
