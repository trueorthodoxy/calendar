---
title: "Март 11"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобнаго отца нашего Софрония, архиепископа Иерусалимскаго.",
"Святаго священномученика Пиония, пресвитера святыя церкве Смирнския в Малой Азии.",
"Преподобнаго отца нашего Георгия Богоносца и чудотворца, иже в церкви святаго Иоанна Богослова в Константинополе.",
"Святых мученик Трофима и Фалла (Фала) Лаодикийских.",
"Святаго мученика Епимаха Египетскаго, егоже святых мощей память обретения совершаем.",
"Преподобныя матере нашея Феодоры Артския, императрицы.",
"Преподобнаго отца нашего Георгия Синаита.",
"Святых преподобномученик Викентия, игумена обители святаго Клавдия в Леоне, во Испании, и Рамира настоятеля и иже с ним дванадесяти иноков.",
"Святаго священномученика Евлогия Кордовскаго.",
"Преподобнаго отца нашего Софрония Затворника Киево-Печерскаго.",
"Иже во святых отца нашего Евфимия, архиепископа Новгородскаго."]
---
