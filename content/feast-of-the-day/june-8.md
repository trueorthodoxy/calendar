---
title: "Июнь 8"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго великомученика Феодора Стратилата, егоже святых мощей память пренесения совершаем (IV)",
"Святых мученик Никандра и Маркиана Доростольских (ок. 303)",
"Святыя мученицы Каллиопии (249–251)",
"Преподобныя матере нашея Мелании Старшия, старицы Римския (ок. 410)",
"Преподобнаго отца нашего Афре (Атры), Нитрийскаго, Египетскаго (V)",
"Святаго мученика Никандра",
"Святаго мученика Марка",
"Святаго Навкратия, игумена Студийскаго (848)",
"Святаго новомученика Феофана Константинопольскаго (1588)",
"Преподобнаго отца нашего Зосимы Финикийскаго (VI)",
"Иже во святых отца нашего Медарда, епископа Нуайонскаго (558-560)",
"Иже во святых отца нашего Феодора, епископа Ростовскаго и Суздальскаго (ок. 1023)",
"Благоверных князей Василия и Константина Ярославских, ихже святых мощей память обретения совершаем (1501)",
"Святых новомученик Российских, братьев епископов Варлаама (1942) и Германа (1937) (Ряшенцевых)"]
---
