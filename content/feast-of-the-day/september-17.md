---
title: "Сентябрь 17"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых и добропобедных мучениц дев Веры, Надежды и Любови, и матере их Софии (ок. 137)",
"Святыя мученицы Агафоклии (230)",
"Святых мученик Максима, Феодота и Асклепиодота Фракийских",
"Святыя Лукии и святаго мученика Герминиана, сына ея, Римских (ок. 303)",
"Святыя мученицы Феодотии Никейския (Понтийския) (230)",
"Святых 100 мученик Египетских, Пелия и Нила, епископов Египетских, Зинона пресвитера, в Палестине пострадавших (310)",
"Святых 50 мученик Египетских, Патермуфия и Илии наиславнейших, в Палестине пострадавших (310)",
"Святых мученик Харалампия и Пантелеимона, и иже с ними",
"Святых священномучеников Ираклидия (I) и Мирона (II), епископов Тамаса Кипрскаго",
"Преподобнаго отца нашего Анастасия, Периотерскаго, инока Кипрскаго (XII)",
"Преподобнаго отца нашего Евсипия Кипрскаго, епископа",
"Святаго священномученика Ламберта, епископа Маастрихтскаго (696 или 704)",
"Преподобномученицы Колумбы, девы Кордовския (853)"]
---
