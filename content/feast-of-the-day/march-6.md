---
title: "Март 6"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых четыредесяти двух мучеников Аморийских: Феодора, Константина, Каллиста, Феофила, Васоя, и иже с ними.",
"Преподобнаго отца нашего Аркадия.",
"Преподобнаго отца нашего Исихия чудотворца Галатийскаго.",
"Святаго преподобномученика Максима.",
"Святаго мученика Евфросина.",
"Святых мученик Иулиана и Еввула.",
"Преподобномученик Конона и сына его Конона, Иконийских.",
"Преподобнаго отца нашего Фридолина, инока Пуатьерскаго, иже в Галлии, и просветителя Швейцарии и Верхняго Рейна.",
"Святаго мученика Авраамия Болгарскаго."]
---
