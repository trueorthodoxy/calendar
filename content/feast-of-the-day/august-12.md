---
title: "Август 12"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых мученик Фотия и Аникиты Никомидийских (305-306)",
"Святых мученик Памфила и Капитона (III)",
"Преподобных отцев наших Сергия и Стефана",
"Преподобнаго отца нашего Кастора",
"Преподобнаго отца нашего Паламона Египетскаго, наставника святаго Пахомия Великаго (ок. 323)",
"Святых двунадесяти мученик воинов Критских (IV)",
"Святаго священномученика Александра, епископа Команскаго (III)",
"Святителя Мюрдаха (Муртага), епископа Киллалийскаго во Ирландии (V-VI)",
"Святых преподобномучеников Геронтия, Серапиона, Германа, Виссариона, Михаила, Симона и Отара Гареджийских во Иверии (1851)"]
---
