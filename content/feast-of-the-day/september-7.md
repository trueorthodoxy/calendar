---
title: "Сентябрь 7"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго мученика Созонта Ликаонскаго (ок. 304)",
"Святых Апостол от 70: Евода, перваго епископа Антиохийскаго (66), и Онисифора (после 67)",
"Святаго мученика Евпсихия из Кесарии Каппадокийския (117-138)",
"Преподобнаго отца нашего Луки, игумена Спасовыя обители, называемыя Глубокия Реки (после 975)",
"Святыя мученицы Регины, девы Отонския (Августодунския) в Галлии (III)",
"Святителя Евуртия, епископа Аврелианскаго (Орлеанскаго) (IV)",
"Святаго Клу, игумена и основателя обители Ножан-на-Сене (560)",
"Иже во святых отца нашего Иоанна чудотворца, архиепископа Новгородскаго (1186)",
"Преподобномученика Макария Каневскаго, архимандрита Овручскаго (1678)",
"Преподобнаго и Богоноснаго отца нашего Макария Оптинскаго (1860)",
"Святаго новосвященномученика Иоанна (Масловскаго), пресвитера из Верхне-Полтавки (1921)"]
---
