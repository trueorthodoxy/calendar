---
title: "Февраль 27"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобнаго отца нашего Прокопия Декаполита, исповедника.",
"Святаго мученика Геласия.",
"Преподобнаго отца нашего Фалалея Сирийского.",
"Преподобнаго отца нашего Стефана, создателя богадельни во Арматии.",
"Святаго мученика Нисия.",
"Преподобных отцев наших Асклипия и Иакова Сирийских.",
"Преподобнаго отца нашего Тимофея Кесарийскаго.",
"Иже во святых отца нашего Леандра, епископа Севильскаго.",
"Преподобных отцев наших Тита пресвитера и Тита бывшаго воина, Киево-Печерских."]
---
