---
title: "Январь 30"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, Собор Еяже иконы Тиносския празднуем (1823)",
"Собор Вселенских учителей и святителей Василия Великаго, Григория Богослова и Иоанна Златоустаго",
"Святаго священномученика Ипполита, папы Римскаго, мученик Кенсорина магистра и с ним 20 стражей темничных; Хрисии девы, Савина, слуги ея, и инех 20 с нею пострадавших: Филикла, Максима, Геркулина, Венерия, Стиракина, Мины, Коммода, Ерма, Мавра, Евсевия, Рустика, Монагрея, Амандина, Олимпия, Кипра, Феодора трибуна, Максима пресвитера, Архелая диакона, Кирина епископа и Максима пресвитера инаго (III)",
"Святаго мученика Феофила Новаго (784)",
"Святаго новомученика Феодора Хаджи Митиленскаго (1784)",
"Благовернаго Петра, царя Болгарскаго (970)",
"Преподобнаго отца нашего Зинона, постника Киево-Печерскаго (XIV)",
"Преподобныя матере нашея Пелагии Дивеевския, Христа ради юродивыя (1884)"]
---
