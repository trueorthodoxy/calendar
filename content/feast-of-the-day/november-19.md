---
title: "Ноябрь 19"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго пророка Авдия (IX до Р.Х.)",
"Святаго мученика Варлаама Антиохийскаго (ок. 304)",
"Святых мученик Азы Исаврийскаго, чудотворца и 150 воинов, иже во Христа чрез него уверовавших (284-305)",
"Святых мучениц жены и дщери епарха",
"Святых 12 воинов, во главу усеченных",
"Святаго мученика Агапия Кесарийскаго в Палестине (ок. 303)",
"Святаго мученика Илиодора Магидскаго в Памфилии (ок. 273)",
"Святых мученик Анфима, Фалалея, Христофора, Евфимии и чад их, и святаго Панхария",
"Преподобнаго отца нашего Симона, чудотворца Калабрийскаго (Х)",
"Преподобнаго отца нашего Илариона, чудотворца Иверскаго (875)",
"Преподобнаго отца нашего Варлаама, игумена Киево-Печерскаго (1065)"]
---
