---
title: "Ноябрь 29"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго мученика Парамона Вифинскаго и с ним 370 мученик (250)",
"Святаго мученика Филумена Анкирскаго (ок. 274)",
"Преподобнаго отца нашего Николая, архиепископа Солунскаго (II)",
"Святаго священномученика Иоанна Персидскаго (343)",
"Святых шести мученик, гонимых и принятых расседшеюся скалою",
"Святителя Урвана, епископа Македонскаго, апостола от 70 (I)",
"Святаго священномученика Дионисия, епископа Коринфскаго (ок. 182)",
"Преподобнаго отца нашего Панкосмия",
"Преподобнаго отца нашего Питируна Египетскаго, ученика преподобнаго Антония Великаго (IV)",
"Святаго мученика Валериана (ок. 274)",
"Святаго мученика Федра (ок. 274)",
"Святаго священномученика Сатурнина, перваго епископа Тулузскаго (III)",
"Святаго священномученика Авива, епископа Некресскаго во Иверии (ок. 552-560)",
"Преподобнаго отца нашего Нектария Послушливаго, Киево-Печерскаго (XII)"]
---
