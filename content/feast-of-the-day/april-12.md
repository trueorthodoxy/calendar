---
title: "Апрель 12"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, пренесение Еяже честнаго пояса мы поминаем, и Собор Еяже иконы Муромския празднуем",
"Иже во святых отца нашего Василия Исповедника, епископа Парийскаго",
"Преподобныя матере нашей Анфусы девы, дщере царя Константина Копронима",
"Святых мученик Дима и Протиона",
"Святаго священномученика Артемона",
"Преподобномученик Мины, Давида и Иоанна Палестинских",
"Преподобнаго отца нашего Акакия Новаго Кавсокаливита",
"Святаго мученика Виктора Брагскаго",
"Иже во святых отца нашего Зинона Исповедника, епископа Веронийскаго",
"Иже во святых отца нашего Василия, епископа Рязанскаго"]
---
