---
title: "Сентябрь 12"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго священномученика Автонома, епископа Италийскаго (313)",
"Святаго священномученика Корнута, епископа Иконийскаго (Никомидийскаго) (249-259)",
"Святаго священномученика Феодора, епископа Александрийскаго",
"Святаго мученика Иулиана Галатийскаго (IV)",
"Святых мученик Македония, Татиана и Феодула Фригийских (361-363)",
"Преподобнаго отца нашего Даниила Фасийскаго (ок. 843)",
"Святаго мученика Океана",
"Святителя Эльбе, епископа Эмлийскаго в Мюнстере (527)",
"Преподобнаго отца нашего Афанасия Старшаго, Серпуховскаго (1401), ученика святаго Сергия Радонежскаго, и Афанасия Младшаго, ученика его (1395)",
"Преподобнаго отца нашего Вассиана Тиксненскаго (1624)",
"Преподобнаго отца нашего Симеона Верхотурскаго, егоже святых мощей память пренесения совершаем (1704)"]
---
