---
title: "Август 21"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго Апостола от 70 Фаддея (ок. 44)",
"Святыя мученицы Вассы и чад ея Феогния, Агапия и Писта, мученик Едесских в Македонии (305-311)",
"Святаго священномученика Привата, епископа Мандскаго в Галлии (260)",
"Иже во святых отца нашего Сармеана, католикоса Карталинскаго, Иверскаго (774 или 779)",
"Святыя Феоклиты чудотворицы из Малыя Азии (829-842)",
"Преподобнаго отца нашего Авраамия Смоленскаго, чудотворца (1220)",
"Преподобнаго отца нашего Ефрема Смоленскаго (после 1238)",
"Преподобнаго отца нашего Авраамия трудолюбиваго, Киево-Печерскаго (XII-XIII)",
"Преподобнаго отца нашего Корнилия, игумена Палеостровскаго (ок. 1420), и ученика его Авраамия (XV)"]
---
