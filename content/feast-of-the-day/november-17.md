---
title: "Ноябрь 17"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Иже во святых отца нашего Григория Чудотворца, епископа Неокесарийскаго (ок. 266-270)",
"Преподобнаго исповедника Лазаря, иконописца (ок. 857)",
"Преподобных отцев наших Захарии Сапожника и Иоанна Константинопольских (VII)",
"Преподобнаго отца нашего Лонгина Египетскаго (IV)",
"Святителей Геннадия (471) и Максима (1482), патриархов Константинопольских",
"Преподобнаго отца нашего Иустина",
"Преподобнаго отца нашего Геннадия Ватопедскаго (XV)",
"Святых мученик Ацискла и Виктории Кордовских (303-305)",
"Святителя Аниана, епископа Орлеанскаго (453)",
"Святителя Григория, епископа Турскаго (594)",
"Преподобныя матере нашея Хильды, игумении Уитбийския (680)",
"Святаго мученика Гоброна, во святом крещении Михаила, князя Иверскаго, и с ним 133 воинов (914)",
"Преподобнаго отца нашего Никона, чудотворца, ученика святаго Сергия Радонежскаго (1426)",
"Святых новосвященномучеников Российских: Геннадия архимандрита, Тимофея, Константина и Феодора пресвитеров и инех с ними"]
---
