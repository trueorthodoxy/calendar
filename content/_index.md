---
title: "Главная"
date: 2021-02-28T01:11:48+07:00
draft: false
---
Нет такого дня в году, когда бы не праздновалась память Святого или Господский или Богородичный праздник. Вспоминая Святых в их дни памяти мы получаем награду, обещанную нашим Господом: _кто принимает пророка, во имя пророка, получит награду пророка; и кто принимает праведника, во имя праведника, получит награду праведника._ (Мф. 10:41). Регулярно призывая Святых мы делаем их частью своей жизни и сами становимся частью их жизни.
