---
title: "Ноябрь 22"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых апостол от 70: Филимона, Архиппа, Онисима и мученицы равноапостольныя Апфии (I)",
"Святых мученик Кикилии (Цецилии) девы Римския, Валериана, Тивуртия и Максима, стража темничнаго (ок. 230 или 288)",
"Святых мученик Марка, Стефана и инаго Марка Антиохийских (290 или IV)",
"Святаго мученика Прокопия, чтеца Палестинскаго (303)",
"Святаго мученика Менигна Парийскаго (250)",
"Преподобнаго отца нашего Агаввы исмаильтянина, Сирийскаго (V)",
"Святых мученик Анфима, Фалалея, Христофора и Евфимии",
"Преподобнаго отца нашего Каллиста Ксанфопула, патриарха Константинопольскаго (1397)",
"Святаго мученика Фаддея",
"Святых мученик Агапиона и Агапия (304), и священномученика Сисина епископа (после 325)",
"Праведнаго Михаила воина, болгарина (866)",
"Благовернаго Ярополка, во святом крещении Петра, князя Владимиро-Волынскаго (1086)",
"Святаго мученика благовернаго Михаила, великаго князя Тверскаго (1318)"]
---
