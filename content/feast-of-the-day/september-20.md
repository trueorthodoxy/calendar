---
title: "Сентябрь 20"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго великомученика Евстафия Плакиды, жены его Феопистии, и чад их Агапия и Феописта (ок. 118)",
"Святых священномучеников Ипатия епископа Ефесскаго и Андрея пресвитера (VIII)",
"Святых исповедников Мартина папы Римскаго (655), Максима Исповедника (662), и учеников его: Анастасия (662), инаго Анастасия (666), Феодора и Евпрепия (650)",
"Святых мученик Артемидора и Фалалея Константинопольских",
"Святаго Иоанна Египетскаго Исповедника и с ним сорока мученик (310)",
"Преподобнаго и богоноснаго отца нашего Иоанна Критскаго (после 1031)",
"Святаго новопреподобномученика Илариона Критскаго (1804)",
"Святых мученик Михаила, князя Черниговскаго, и Феодора, болярина его, чудотворцев (1245)",
"Преподобнаго отца нашего Олега, великаго князя Брянскаго (1307)"]
---
