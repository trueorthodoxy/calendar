---
title: "Январь 5"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых мученик Феопемпта, епископа Никомидийскаго, и Феоны (303)",
"Преподобныя матере нашея Синклитикии Александрийския (ок. 350)",
"Преподобнаго отца нашего Григория Акритскаго (ок. 820)",
"Преподобнаго отца нашего Фостирия пустынника (VI)",
"Святаго мученика Саиса",
"Святаго мученика Феоида (313)",
"Преподобныя матере нашея Домнины (Домны) (V)",
"Преподобныя матере нашея Татианы",
"Святаго новопреподобномученика Романа из Карпенисия (1694)",
"Святаго новосвященномученика Романа Лакедемонца (1695)"]
---
