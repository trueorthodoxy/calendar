---
title: "Январь 20"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобнаго отца нашего Евфимия Великаго, Палестинскаго (473)",
"Святых мученик Васса, Евсевия, Евтихия и Василида Никомидийских (303)",
"Святых мученик Инны, Пинны и Риммы, скифов (I-II)",
"Праведнаго Петра Константинопольскаго, бывшаго мытаря (VI)",
"Святых мученик Фирса и Агнии (V)",
"Благовернаго царя Льва Великаго, исповедника (474)",
"Святыя мученицы Анны Римския",
"Святаго новомученика Захарии в Патрех Морейских (1782)",
"Преподобных отцев наших Евфимия молчальника (XIV) и Лаврентия затворника (XIII-XIV) Киево-Печерских",
"Преподобнаго отца нашего Евфимия Сянжемскаго, Вологодскаго (ок. 1465)"]
---
