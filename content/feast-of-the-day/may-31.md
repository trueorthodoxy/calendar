---
title: "Май 31"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго мученика Ермия Команскаго, Каппадокийскаго (ок. 138 - 161)",
"Святаго мученика волхва,  иже даде яд святому Ермию, и уверова во Христа чрез него (ок. 138 - 161)",
"Святых пяти мученик Аскалонских (308-309)",
"Святых мученик Евсевия и Харалампия",
"Святых новомученик Михаила, Великаго Князя и Николая (Джонсона), вернаго друга его (1918)",
"Святаго новопреподобномученика Серафима, священноинока",
"Святаго новосвященномученика Иерофея (Афонина), епископа Никольскаго (1928)",
"Святаго новосвященномученика Николая, диакона Лесбосскаго, егоже святых мощей память обретения совершаем (1960)"]
---
