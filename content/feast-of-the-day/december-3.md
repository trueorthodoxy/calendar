---
title: "Декабрь 3"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго пророка Софонии (635-605 до Р.Х.)",
"Святаго священномученика Феодора, архиепископа Александрийскаго (609)",
"Преподобнаго отца нашего Феодула Константинопольскаго, столпника Едесскаго (ок. 404) и святаго Корнилия мима",
"Преподобнаго отца нашего Иоанна молчальника, Савваита, бывшаго епископа Колонийскаго (558)",
"Святых мученик Агапия, Селевка и Маманта",
"Преподобнаго отца нашего Феодула Кипрскаго, Христа ради юродиваго (755)",
"Святаго священномученика Гавриила, епископа Прусскаго (1659)",
"Святаго новомученика Ангелиса Хиосскаго (1813)",
"Святаго мученика Кассиана Танжерскаго (298)",
"Иже во святых отца нашего Вирина, епископа Дорчестерскаго и апостола Уэссекса  (650)",
"Преподобнаго отца нашего Саввы Сторожевкаго, Звенигородскаго, ученика святаго Сергия Радонежскаго (1406)"]
---
