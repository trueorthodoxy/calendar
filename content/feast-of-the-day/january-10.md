---
title: "Январь 10"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Иже во святых отца нашего Григория, епископа Нисскаго (ок. 395)",
"Иже во святых отца нашего Дометиана, епископа Мелитинскаго (601)",
"Преподобнаго отца нашего Маркиана, пресвитера и иконома Великия церкве в Константинополе (ок. 472-474)",
"Преподобнаго отца нашего Аммония Долгаго (ок. 403)",
"Иже во святых отца нашего Мильтиада, папы Римскаго (314)",
"Преподобных отцев наших Павла Обнорскаго (1429) и ученика его Макария Писемскаго (XIV)"]
---
