---
title: "Ноябрь 28"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго преподобномученика и исповедника Стефана Новаго и иже с ним Андрея, Петра, Анны, Василия, Стефана, Григория, инаго Григория и Иоанна (767)",
"Святаго мученика Иринарха Севастийскаго и святых семи жен с ним (303)",
"Иже во святых отца нашего Феодора, епископа Феодосипольскаго во Армении (кон. VI)",
"Святых мученик Тивериопольских: Тимофея и Феодора епископов, Петра, Иоанна, Сергия, Феодора и Никифора пресвитеров, Василия и Фомы диаконов, Иерофея, Даниила, Харитона, Сократа, Комасия, Евсевия и Етимасия иноков (361)",
"Иже во святых отца нашего Феодора, архиепископа Ростовскаго (1394)"]
---
