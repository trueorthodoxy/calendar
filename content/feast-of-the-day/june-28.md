---
title: "Июнь 28"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, Собор Еяже иконы именуемыя Троеручица празднуем (VIII)",
"Святых безсребреников и чудотворцев Кира и Иоанна, ихже святых мощей память пренесения совершаем (412)",
"Святаго мученика Паппия (303-305)",
"Блаженнаго Сергия Магистрата, Панфлагонийскаго, основателя обители Пресвятыя Богородицы в Никомидийстем заливе (866)",
"Святаго мученика Македония",
"Преподобнаго отца нашего Улкиана",
"Святаго Павла, врача Коринфскаго (VII)",
"Святых двух чад, иже распяты бысть",
"Преподобнаго отца нашего Моисея Анахорета (IV-V)",
"Святаго священномученика Донага, епископа Ливийскаго",
"Святых 70 мученик Скифопольских",
"Святых триех мученик Галатийских",
"Преподобнаго отца нашего Магна",
"Преподобномученика Аргимира Кордовскаго (856)",
"Преподобнаго отца нашего Ксенофонта Робейскаго (1262)",
"Преподобных отцев наших Сергия и Германа Валаамских, чудотворцев (XIV)",
"Святаго новосвященномученика Иосифа Дамаскина (1860)"]
---
