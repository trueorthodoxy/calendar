---
title: "Январь 31"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых чудотворцев и безсребреников Кира и Иоанна (311)",
"Святых мучениц Афанасии и дщерей ея: Феоктисты, Феодотии, и Евдоксии (311)",
"Святых мученик Викторина, Виктора, Никифора, Клавдия, Диодора, Серапиона и Папия Коринфских (251)",
"Святыя мученицы Трифены Кизическия (I)",
"Святаго новомученика Илии (Ардуниса) Афонскаго (1686)",
"Иже во святых отца нашего Геминиана, епископа Моденскаго (ок. 396)",
"Иже во святых отца нашего Никиты, затворника Киево-Печерскаго, епископа Новгородскаго (1108)",
"Преподобнаго отца нашего Пахомия Кенскаго (1515)"]
---
