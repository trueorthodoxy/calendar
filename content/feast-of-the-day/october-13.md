---
title: "Октябрь 13"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, собор Еяже иконы именуемыя Вратарница (Иверская) празднуем (1648)",
"Святых мученик Карпа, епископа Фиатирскаго, Папилы диакона, Агафодора, слуги их, и Агафоники, сестры Папилы (ок. 251)",
"Святаго мученика Флорентия Солунскаго (I-II)",
"Святаго мученика Диоскора Александрийскаго (Кинопольскаго) во Египте (ок. 305)",
"Преподобнаго отца нашего Никиты Пафлагонскаго, исповедника (ок. 838)",
"Святаго мученика Вениамина Персидскаго, диакона (418 или 424)",
"Святаго мученика Антигона",
"Святыя новомученицы Златы Могленския (1795)",
"Святых мученик Фавста, Иануария и Марциала Кордовских (304)",
"Преподобнаго отца нашего Ванантия Турскаго, игумена обители святаго Мартина (V)",
"Преподобнаго отца нашего Вениамина Киево-Печерскаго (XIV)",
"Святаго новопреподобномученика Иакова Хаматурскаго, иже в Триполи во главу усеченнаго (XIII)",
"Преподобнаго отца нашего Антония, митрополита Чкондидскаго во Иверии (1815), и его ученика Иакова, священноинока",
"Святых новосвященномучеников Вениамина (Кононова), архимандрита Соловецкаго (1928) и Вениамина (Фролова), епископа Уфимскаго (1936)"]
---
