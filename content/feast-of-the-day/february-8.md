---
title: "Февраль 8"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго и славнаго великомученика Феодора Стратилата (319)",
"Святаго пророка Захарии Серповидца (ок. 520 до Р.Х.)",
"Святых мученик Никифора и Стефана",
"Святых мучениц сестер Марфы и Марии и брата их мученика Ликариона отрока Египетских",
"Святых мученик Филадельфа и Поликарпа",
"Преподобнаго отца нашего Макария, епископа Пафскаго (1688)",
"Святаго мученика Пергета",
"Преподобныя матере нашея Ельфледы, игумении Уитбийския (713-714)",
"Иже во святых отца нашего Саввы Втораго, архиепископа Сербскаго (1271)",
"Святых новосвященномучеников Феодора, пресвитера Голышмановскаго (1918), и Феодора (Поздеевскаго), архиепископа Волоколамскаго (1935 или после 1940)"]
---
