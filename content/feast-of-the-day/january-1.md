---
title: "Январь 1"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Обрезание Господа и Бога и Спаса нашего Иисуса Христа",
"Иже во святых отца нашего Василия Великаго, архиепископа Кесарии Каппадокийския (379)",
"Святаго мученика Феодота, во главу усеченнаго",
"Святителя Григория, епископа Назианзинскаго, отца святителя Григория Богослова (374)",
"Преподобнаго отца нашего Феодосия, игумена Триглийскаго (VIII)",
"Святаго новомученика Петра Триполийскаго, Пелопоннесскаго (1772 или 1776)",
"Преподобнаго отца нашего Евгенда, игумена Кондатскаго в горах Юры (510)",
"Иже во святых отца нашего Феликса, епископа Буржскаго (580)",
"Святаго новомученика Василия Младшаго Ставропольскаго"]
---
