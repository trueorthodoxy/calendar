---
title: "Июнь 15"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго пророка Амоса (VIII до Р.Х.)",
"Святаго мученика Дулы Киликийскаго (ок. 305-313)",
"Святаго мученика Нерсеса",
"Святых Апостол Фортуната, Ахаика и Стефана (I)",
"Преподобнаго отца нашего Ортисия Тавеннисийскаго, ученика святаго Пахомия Великаго (ок. 380)",
"Святыя мученицы, старицы",
"Святых мученик Луканийских (Сицилийских): Вита, Модеста и Крискентии питательницы (303)",
"Преподобнаго отца нашего Авраама Овернийскаго, игумена Клермонскаго в Галлии (477-480)",
"Святыя мученицы Бенильды Кордовския (853)",
"Иже во святых отца нашего Михаила, перваго митрополита Киевскаго и всея Руси (992)",
"Святаго мученика Лазаря, благовернаго князя Сербскаго (1389)",
"Святителей Ефрема (1400) и Спиридона Песнописца (1388), патриархов Сербских",
"Преподобномученик Григория и Кассиана Авнежских, ихже святых мощей память обретения совершаем (1524)",
"Иже во святых отца нашего Симеона, архиепископа Новгородскаго (1421)",
"Иже во святых отца нашего Ионы, митрополита Московскаго и всея Руси, чудотворца (1461)",
"Иже во святых отца нашего Гликерия, митрополита Румынскаго (1985)"]
---
