---
title: "Сентябрь 21"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго апостола от 70 Кодрата (ок. 117 или 130)",
"Святаго пророка Ионы (VIII до Р.Х.)",
"Преподобнаго отца нашего Ионы (VIII - IX), иже в обители Саввы Освященнаго, отца святых Феофана творца канонов и Феодора, Начертанных",
"Святаго мученика Евсевия Финикийскаго (ок. 360-363)",
"Святых мученик братьев Евсевия, Нестава и Зинона, Газских (ок. 361-362)",
"Иже во святых отец наших Мелетия и Исаакия, епископов Кипрских",
"Святаго мученика Приска Фригийскаго",
"Святых шести мученик, царя Максимиана стражей",
"Святителя Кастора, епископа Аптскаго в Галлии (до 426)",
"Преподобнаго отца нашего Даниила Шужгорскаго (XVI)",
"Преподобнаго отца нашего Иосифа Заоникиевскаго (1612)",
"Иже во святых отца нашего Дмитрия, митрополита Ростовскаго, егоже святых мощей память обретения совершаем (1752)"]
---
