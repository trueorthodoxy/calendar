---
title: "Август 3"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобных отцев наших Далмата (после 446), Фавста (V) и Исаакия (383) Константинопольских",
"Святаго священномученика Стефана, папы Римскаго, и инех с ним (257)",
"Преподобнаго отца нашего Иоанна Исповедника, игумена Паталарейскаго (VIII-IX)",
"Святыя мироносицы Саломии (I)",
"Преподобныя матере нашея Феоклиты Чудотворицы (829-842)",
"Святаго Раждена Персиянина, первомученика Иверскаго (457)",
"Преподобнаго отца нашего Антония Римлянина, Новгородскаго чудотворца (1147)",
"Святаго новопреподобномученика Исаакия Саровскаго"]
---
