---
title: "Январь 21"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, Собор Еяже икон Одигитрия и Утешение Ватопедския (807) празднуем",
"Преподобнаго отца нашего Максима Исповедника (662)",
"Святаго мученика Неофита Никейскаго (303-305)",
"Преподобнаго отца нашего Зосимы, епископа Сиракузскаго (662)",
"Святых мученик Евгения, Кандида, Валериана и Акилы Трапезундских (303)",
"Святыя великомученицы Ирины, собор еяже церкви близ моря празднуем",
"Святыя мученицы Агнии (Агнессы) девы Римския (ок. 304)",
"Четырех мученик Тирских",
"Преподобнаго отца нашего Неофита Ватопедскаго (XIV)",
"Святых священномучеников Фруктуоза, епископа Таррагонскаго, и с ним Авгурия и Евлогия диаконов (259)",
"Иже во святых отца нашего Епифания, епископа Павскаго (496)",
"Святаго преподобномученика Мейнарда Айнзидельнскаго (861)",
"Преподобнаго отца нашего Максима Грека (1556)"]
---
