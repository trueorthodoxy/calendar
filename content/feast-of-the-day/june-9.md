---
title: "Июнь 9"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Иже во святых отца нашего Кирилла, архиепископа Александрийскаго (444)",
"Святых пяти дев мучениц: Феклы, Мариамны, Марфы, Марии и Еннафы Персидских, Павлом пресвитером их во главу усеченных (346-374)",
"Преподобнаго отца нашего Кира",
"Святаго священномученика Александра, епископа Прусскаго (до IV)",
"Святаго мученика Анании",
"Святых триех дев мучениц Хиосских",
"Святаго священномученика Викентия, диакона Аженскаго в Галлии (ок. 300)",
"Преподобнаго отца нашего Колумбы, чудотворца Ионскаго (597)",
"Преподобнаго отца нашего Иоанна Шавтели, епископа Гаенатскаго, Иверскаго (XII-XIII)",
"Преподобнаго отца нашего Кирилла, игумена Белоезерскаго, чудотворца (1427)",
"Преподобнаго отца нашего Александра, игумена Куштскаго (1439)"]
---
