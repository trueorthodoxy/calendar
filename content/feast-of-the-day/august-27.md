---
title: "Август 27"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобнаго отца нашего Пимена Великаго, Египетскаго (ок. 450)",
"Преподобнаго отца нашего Ливерия исповедника, папы Римскаго (366)",
"Святителя Осии исповедника, епископа Кордовскаго (359)",
"Евнуха Ефиопскаго, иже от Апостола Филиппа крещеннаго (I)",
"Святыя мученицы Анфусы (Анфисы) Новыя",
"Святаго великомученика Фанурия Новоявленнаго, Родосскаго (II-IV)",
"Святителя Авксилия, епископа Киллосскаго, сподвижника святаго Патрикия (ок. 460)",
"Преподобных отцев наших Кукши священномученика и Пимена постника, Киево-Печерских (после 1114)",
"Святителей Феогноста, Киприана и Фотия, митрополитов Московских, ихже святых мощей память пренесения совершаем"]
---
