---
title: "Август 8"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Пресвятыя Владычицы нашея Богородицы и Приснодевы Марии, Собор Еяже иконы Толгския празднуем (1314)",
"Преподобнаго отца нашего Емилиана исповедника, епископа Кизическаго (ок. 815-820)",
"Святаго Мирона чудотворца, епископа Критскаго (ок. 350)",
"Преподобнаго отца нашего Феодосия, игумена Оровскаго",
"Святых мученик Елевферия и Леонида Константинопольских, и с ними многих младенцев (IV)",
"Святых десяти подвижников Египетских",
"Святых двух мученик Тирских",
"Святаго мученика Стиракия",
"Святаго новомученика Триандафила Солунскаго, Загорскаго, Константинопольскаго (1680)",
"Святаго новомученика Анастасия Болгарина, Струмицкаго, Солунскаго (1794)",
"Преподобнаго отца нашего Колмана Линдисфарнскаго (676)",
"Преподобных отцев наших Григория иконописца (XII) и Григория чудотворца (1093), Киево-Печерских",
"Преподобных отцев наших Зосимы (1478) и Савватия (1435) Соловецких, ихже святых мощей память пренесения совершаем (1566 и 1992)",
"Преподобнаго отца нашего Евфимия Гареджийскаго, игумена обители святаго Иоанна Крестителя (1804)"]
---
