---
title: "Июнь 19"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святаго Апостола Иуды, брата Господня (ок. 80)",
"Святаго мученика Зосимы из Антиохии Писидийския (II)",
"Преподобнаго отца нашего Зинона, пустынника Египетскаго, ученика святаго Сильвана Палестинскаго (кон. IV)",
"Преподобнаго отца нашего Паисия Великаго, Египетскаго (400)",
"Святаго священномученика Асинкрита",
"Преподобнаго отца нашего Варлаама Важскаго, Шенкурскаго (1462)",
"Иже во святых отца нашего Иоанна (Максимовича), архиепископа Шанхайскаго и Сан-Францисскаго, чудотворца (1966)"]
---
