---
title: "Декабрь 13"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Святых и славных мученик Великия Армении: Евстратия, Авксентия, Евгения, Мардария и Ореста (284-305)",
"Святыя мученицы Лукии, девы Сиракузския (304 или 310)",
"Преподобнаго отца нашего Арсения, иже в Латре (VIII-XI)",
"Святаго новосвященномученика Гавриила, архиепископа Сербскаго (1659)",
"Преподобнаго отца нашего Ариса Египетскаго",
"Преподобнаго отца нашего Мардария, затворника Киево-Печерскаго (XIII)"]
---
