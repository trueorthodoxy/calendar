---
title: "Февраль 7"
date: 2021-02-28T09:07:48+07:00
draft: false
saints: ["Преподобнаго отца нашего Парфения, епископа Лампсакийскаго (после 325)",
"Преподобнаго отца нашего Луки Стиридскаго (946 или 953)",
"Святых 1003 мученик Никомидийских, иже во Христа чрез святителя Петра Александрийскаго уверовавших (IV)",
"Святых шести мученик фригийских (ок. 305)",
"Преподобнаго отца нашего Петра Моноватийскаго",
"Святаго мученика Феопемпта с братиями",
"Иже во святых отца нашего Априона, епископа Кипрскаго",
"Святаго и славнаго новомученика Георгия Критскаго (1867)",
"Святителя Моисея, епископа и просветителя арабов (IV)",
"Святаго Рихарда, короля Уессекскаго (ок. 720)"]
---
